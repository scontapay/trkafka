from kafka import KafkaConsumer
from protobuf import EmployeeEvent_pb2
import boto3


def decrypt(client, secret, event_type):
    plaintext = client.decrypt(
        CiphertextBlob=secret
    )
    if event_type == 'PendingHireEvent':
        return EmployeeEvent_pb2.PendingHireEvent.FromString(plaintext["Plaintext"])
    elif event_type == 'VeteranSelfIdentificationUpdatedEvent':
        return EmployeeEvent_pb2.VeteranSelfIdentificationUpdatedEvent
    elif event_type == 'EmployeeCustomFieldsChangedEvent':
        return EmployeeEvent_pb2.EmployeeCustomFieldsChangedEvent
    elif event_type == 'EmployeeDemographicsEthnicityUpdatedEvent':
        return EmployeeEvent_pb2.EmployeeDemographicsEthnicityUpdatedEvent.FromString(plaintext["Plaintext"])


if __name__ == "__main__":
    # start the program
    print('Running the consumer on localhost...')
    topic_name = 'employee_event_created_event'
    print('create some kafka consumer')
    consumer = KafkaConsumer(topic_name, auto_offset_reset='earliest',
                             bootstrap_servers=['localhost:9092'], api_version=(0, 10),
                             consumer_timeout_ms=1000)
    # decrypt stuff
    decrypt_client = boto3.client('kms',
                                  aws_access_key_id='<use id>',
                                  aws_secret_access_key='<use secret>')
    counter = 0
    for msg in consumer:
        print('Message #{0} from topic'.format(counter))
        print(msg.value)
        proto_msg = EmployeeEvent_pb2.EmployeeEvent.FromString(msg.value)
        print('Protobuf message deconstructed')
        print('Event Type: {0}'.format(proto_msg.event_type))
        print(proto_msg)
        decrypted_event = decrypt(decrypt_client, proto_msg.event_body, proto_msg.event_type)
        print('inner event type: {0} decrypted inner event body: {1}'.format(proto_msg.event_type, decrypted_event))
        counter = counter + 1
        if counter > 1:
            break

    consumer.close()
